﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Archeologia.Repository.Models
{
    public class MapPointViewModel
    {
        public int Id { get; set; }
        public string Miejscowosc { get; set; }
        public string NumerStanowiska { get; set; }
        public string Gmina { get; set; }
        public string Powiat { get; set; }
        public string Wojewodztwo { get; set; }
        public string Autor { get; set; }
        public string Funkcja { get; set; }
        public string Kultura { get; set; }
        public string Chronologia { get; set; }
        public byte[] InneDane { get; set; }
    }
}
