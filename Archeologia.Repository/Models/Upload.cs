﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Archeologia.Repository.Models
{
    public class Upload
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public byte[] Contents { get; set; }

        public int WspolrzedneId { get; set; }
    }
}
