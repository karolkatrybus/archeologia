﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Archeologia.Repository.Models
{
    public class MapPolygon
    {
        public int Id { get; set; }
        public string Geometria { get; set; }
    }
}
