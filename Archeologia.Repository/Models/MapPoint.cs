﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Archeologia.Repository.Models
{
    public class MapPoint
    {
        public int Id { get; set; }
        [Display(Name = "Wspołrzędne")]
        public string Geometria { get; set; }

        //KARTA EWIDENCYJNA
        public bool A { get; set; }
        [Display(Name = "Odkrycia terenowe")]
        public bool T { get; set; }
        [Display(Name = "Weryfikacja terenowa niemożliwa")]
        public bool X { get; set; }
        [Display(Name = "Stanowisko nieistniejące")]
        public bool N { get; set; }
        [Display(Name = "Badania wykopaliskowe")]
        public bool W { get; set; }
        [Display(Name = "Badania powierzchniowe")]
        public bool P { get; set; }
        [Display(Name = "Odkrycia luźne, przypadkowe")]
        public bool L { get; set; }


        //LOKALIZACJA
        [StringLength(50)]
        [Display(Name = "Miejscowość")]
        public string Miejscowosc { get; set; }
        [Display(Name = "Numer stanowiska")]
        public int NumerStanowiska { get; set; }
        public string Gmina { get; set; }
        public string Powiat { get; set; }
        public string Wojewodztwo { get; set; }
        [RegularExpression("[0-9]{2}-[0-9]{3}")]
        [Display(Name = "Numer obszaru AZP")]
        public string AzpObszar { get; set; }
        [Display(Name = "Numer stanowiska na obszarze AZP")]
        public int AzpStanowiska { get; set; }
        [Display(Name = "Nazwa lokalna")]
        public string NazwaLokalna { get; set; }
        [Display(Name = "Numer działki geodezyjnej")]
        public int NumerDzialki { get; set; }
        [Display(Name = "Identyfikator EGB")]
        public int Identyfikator { get; set; }
        [Display(Name = "Właściciel")]  //drop box: Skarb Państwa, samorząd, osoba prawna, osoba fizyczna (prywatna), związek wyznaniowy, własność nieuregulowana
        public string Wlasciciel { get; set; }

        //POŁOŻENIE FIZYCZNOGEOGRAFICZNE
        [Display(Name = "Jednostka fizycznogeograficzna (mezoregion)")]
        public string Jednostka { get; set; }

        //UTWÓR GEOLOGICZNY
        public bool Luzny { get; set; }
        public bool Zwiezly { get; set; }
        public bool Torfbagn { get; set; }
        public string OkreslenieSpecjalistyczne { get; set; }

        //DOSTĘPNOŚĆ TERENU
        public bool Niezabudowany { get; set; }
        public bool SrednioZabudowany { get; set; }
        public bool Zabudowany { get; set; }
        public bool PoleOrne { get; set; }
        public bool Nieuzytek { get; set; }
        public bool Laka { get; set; }
        public bool Sad { get; set; }
        public bool Park { get; set; }
        public bool Las { get; set; }
        public bool Torf { get; set; }
        public bool Bagno { get; set; }
        public bool Woda { get; set; }

        //KLASYFIKACJA FUNKCJONALNO-KULTUROWO-CHRONOLOGICZNA STANOWISKA
        public string Funkcja { get; set; }
        public string Kultura { get; set; }
        public string Chronologia { get; set; }
        public string Opis { get; set; }

        //OPIS OBSZARU STANOWISKA
        public bool ObserwacjaUtrudniona { get; set; }
        public bool BezPrzeszk { get; set; }
        public bool PoleOtwarte { get; set; }
        public bool PoleZamkniete { get; set; }
        public bool NasycenieRownomierne { get; set; }
        public bool NasycenieJednocentryczne { get; set; }
        public bool NasycenieWielocentryczne { get; set; }
        public string Powierzchnia { get; set; }
        public string Gestosc { get; set; } //drop box: duża (ponad 10 znalezisk na pow 1a, średnia (4-10 na 1a), mała (1-3 na 1a)

        //ZAGROŻENIA
        public string Zagrozenia { get; set; }

        //WNIOSKI KONSERWATORSKIE
        [Display(Name = "Niezbędna szczegółowa inwentaryzacja")]
        public bool Inwentaryzacja { get; set; }
        [Display(Name = "Niezbędne badania wykopaliskowe")]
        public bool Badania { get; set; }
        [Display(Name = "Niezbędna interwencja administracyjna")]
        public bool Interwencja { get; set; }

        //AKTUALNA OCHRONA
        [Display(Name = "Nr rejestru zabytków")]
        public int NumerRejestru { get; set; }
        [Display(Name = "Data wpisu do rejestru")]
        [DataType(DataType.Date)]
        public DateTime DataRejestru { get; set; }
        [Display(Name = "Park kulturowy")]
        public bool ParkKulturowy { get; set; }
        [Display(Name = "Plan zagospodarowania przestrzennego")]
        public bool PlanZagospodarowania { get; set; }

        //OPRACOWANIE KARTY EWIDENCYJNEJ
        [Display(Name = "Data przeprowadzenia badań")]
        [DataType(DataType.Date)]
        public DateTime DataOpracowania { get; set; }
        [Display(Name = "tytuł naukowy, imię i nazwisko autora")]
        [StringLength(100)]
        public string Autor { get; set; }
        [Display(Name = "tytuł naukowy, imię i nazwisko konsultanta")]
        [StringLength(100)]
        public string Konsultant { get; set; }
        [Display(Name = "tytuł naukowy, imię i nazwisko konsultanta sprawdzającego poprawnosć")]
        [StringLength(100)]
        public string KonsultantAzp { get; set; }

        //WERYFIKACJA KONSERWATORSKA
        [Display(Name = "Potwierdzenie wojewódzkiego konserwatora zabytków")]
        public string AkceptacjaWkz { get; set; }
        [Display(Name = "Miejscowość, Data")]
        public string MiejscowoscData { get; set; }

        //ZBIORY I NR INWENTARZA  
        [Display(Name = "Miejsca przechowywania zabytków ruchomych")]
        public string Miejsce { get; set; }
        [Display(Name = "Numer inwentarza")]
        public int NumerInwentarza { get; set; }

        //DOKUMENTACJA
        public string Dokumentacja { get; set; }

        //LITERATURA
        public string Literatura { get; set; }

        //INNE DANE IFormFile 
        public ICollection<Upload> InneDane { get; set; }
    }
}
