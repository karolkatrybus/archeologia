﻿using Archeologia.Repository.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace Archeologia.Repository.DatabaseQueries
{
    public class SelectGisData
    {
        private string connectionString;
        private DataSet dataSet = new DataSet();
        private DataTable dataTable = new DataTable();

        public SelectGisData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public int GetPointsCount()
        {
            var count = 0;
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("select count(*) FROM wspolrzedne", connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        count = Convert.ToInt32(row[0]);
                    }
                }
            }

            return count;
        }

        public int GetPolygonCount()
        {
            var count = 0;
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("select count(*) FROM terenybadawcze", connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        count = Convert.ToInt32(row[0]);
                    }
                }
            }

            return count;
        }

        public int GetImagesCount()
        {
            var count = 0;
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("select count(*) FROM innedane", connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        count = Convert.ToInt32(row[0]);
                    }
                }
            }
            return count;
        }

        public MapPoint GetPointById(int id)
        {
            var newPoint = new MapPoint();
            newPoint.Id = id;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var query = String.Format("select a,t,x,n,w,p,l from kartaewidncyjna where id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.A = Convert.ToBoolean(row["a"]);
                        newPoint.T = Convert.ToBoolean(row["t"]);
                        newPoint.X = Convert.ToBoolean(row["x"]);
                        newPoint.N = Convert.ToBoolean(row["n"]);
                        newPoint.W = Convert.ToBoolean(row["w"]);
                        newPoint.P = Convert.ToBoolean(row["p"]);
                        newPoint.L = Convert.ToBoolean(row["l"]);
                    }
                }

                query = String.Format("select miejscowosc, numer_stanowiska, gmina, powiat, wojewodztwo, azp_obszar, azp_stanowiska, nazwa_lokalna, numer_dzialki, identyfikator, wlasciciel FROM lokalizacja WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Miejscowosc = Convert.ToString(row["miejscowosc"]);
                        newPoint.NumerStanowiska = Convert.ToInt32(row["numer_stanowiska"]);
                        newPoint.Gmina = Convert.ToString(row["gmina"]);
                        newPoint.Powiat = Convert.ToString(row["powiat"]);
                        newPoint.Wojewodztwo = Convert.ToString(row["wojewodztwo"]);
                        newPoint.AzpObszar = Convert.ToString(row["azp_obszar"]);
                        newPoint.AzpStanowiska = Convert.ToInt32(row["azp_stanowiska"]);
                        newPoint.NazwaLokalna = Convert.ToString(row["nazwa_lokalna"]);
                        newPoint.NumerDzialki = Convert.ToInt32(row["numer_dzialki"]);
                        newPoint.Identyfikator = Convert.ToInt32(row["identyfikator"]);
                        newPoint.Wlasciciel = Convert.ToString(row["wlasciciel"]);
                    }
                }

                query = String.Format("select jednostka FROM polozenie WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Jednostka = Convert.ToString(row["jednostka"]);
                    }
                }

                query = String.Format("select luzny, zwiezly, torfbagn, okreslenie_specjalistyczne FROM utworgeologiczny WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Luzny = Convert.ToBoolean(row["luzny"]);
                        newPoint.Zwiezly = Convert.ToBoolean(row["zwiezly"]);
                        newPoint.Torfbagn = Convert.ToBoolean(row["torfbagn"]);
                        newPoint.OkreslenieSpecjalistyczne = Convert.ToString(row["okreslenie_specjalistyczne"]);
                    }
                }

                query = String.Format("select niezabudowany, srednio_zabudowany, zabudowany, pole_orne, nieuzytek, laka, sad, park, las, torf, bagno, woda FROM dostepnosc WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Niezabudowany = Convert.ToBoolean(row["niezabudowany"]);
                        newPoint.SrednioZabudowany = Convert.ToBoolean(row["srednio_zabudowany"]);
                        newPoint.Zabudowany = Convert.ToBoolean(row["zabudowany"]);
                        newPoint.PoleOrne = Convert.ToBoolean(row["pole_orne"]);
                        newPoint.Nieuzytek = Convert.ToBoolean(row["nieuzytek"]);
                        newPoint.Laka = Convert.ToBoolean(row["laka"]);
                        newPoint.Sad = Convert.ToBoolean(row["sad"]);
                        newPoint.Park = Convert.ToBoolean(row["park"]);
                        newPoint.Las = Convert.ToBoolean(row["las"]);
                        newPoint.Torf = Convert.ToBoolean(row["torf"]);
                        newPoint.Bagno = Convert.ToBoolean(row["bagno"]);
                        newPoint.Woda = Convert.ToBoolean(row["woda"]);
                    }
                }

                query = String.Format("select funkcja, kultura, chronologia, opis FROM klasyfikacja WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Funkcja = Convert.ToString(row["funkcja"]);
                        newPoint.Kultura = Convert.ToString(row["kultura"]);
                        newPoint.Chronologia = Convert.ToString(row["chronologia"]);
                        newPoint.Opis = Convert.ToString(row["opis"]);
                    }
                }

                query = String.Format("select obserwacja_utrudniona, bez_przeszkodzona, pole_otwarte, pole_zamkniete, nasycenie_rownomierne, nasycenie_jednocentryczne, nasycenie_wielocentryczne, powierzchnia, gestosc FROM opis WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.ObserwacjaUtrudniona = Convert.ToBoolean(row["obserwacja_utrudniona"]);
                        newPoint.BezPrzeszk = Convert.ToBoolean(row["bez_przeszkodzona"]);
                        newPoint.PoleOtwarte = Convert.ToBoolean(row["pole_otwarte"]);
                        newPoint.PoleZamkniete = Convert.ToBoolean(row["pole_zamkniete"]);
                        newPoint.NasycenieRownomierne = Convert.ToBoolean(row["nasycenie_rownomierne"]);
                        newPoint.NasycenieJednocentryczne = Convert.ToBoolean(row["nasycenie_jednocentryczne"]);
                        newPoint.NasycenieWielocentryczne = Convert.ToBoolean(row["nasycenie_wielocentryczne"]);
                        newPoint.Powierzchnia = Convert.ToString(row["powierzchnia"]);
                        newPoint.Gestosc = Convert.ToString(row["gestosc"]);
                    }
                }

                query = String.Format("select tekst FROM zagrozenia WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Zagrozenia = Convert.ToString(row["tekst"]);
                    }
                }

                query = String.Format("select inwentaryzacja, badania, interwencja FROM wnioski WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Inwentaryzacja = Convert.ToBoolean(row["inwentaryzacja"]);
                        newPoint.Badania = Convert.ToBoolean(row["badania"]);
                        newPoint.Interwencja = Convert.ToBoolean(row["interwencja"]);
                    }
                }

                query = String.Format("select numer_rejestru, data, park_kulturowy, plan_zagospodarowania FROM ochrona WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.NumerRejestru = Convert.ToInt32(row["numer_rejestru"]);
                        newPoint.DataRejestru = Convert.ToDateTime(row["data"]);
                        newPoint.ParkKulturowy = Convert.ToBoolean(row["park_kulturowy"]);
                        newPoint.PlanZagospodarowania = Convert.ToBoolean(row["plan_zagospodarowania"]);
                    }
                }

                query = String.Format("select data, autor, konsultant, konsultantazp FROM opracowanie WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.DataOpracowania = Convert.ToDateTime(row["data"]);
                        newPoint.Autor = Convert.ToString(row["autor"]);
                        newPoint.Konsultant = Convert.ToString(row["konsultant"]);
                        newPoint.KonsultantAzp = Convert.ToString(row["konsultantazp"]);
                    }
                }

                query = String.Format("select akceptacjawkz, miejscowosc_data FROM weryfikacja WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.AkceptacjaWkz = Convert.ToString(row["akceptacjawkz"]);
                        newPoint.MiejscowoscData = Convert.ToString(row["miejscowosc_data"]);
                    }
                }

                query = String.Format("select miejsce, numer_inwentarza FROM zbiory WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Miejsce = Convert.ToString(row["miejsce"]);
                        newPoint.NumerInwentarza = Convert.ToInt32(row["numer_inwentarza"]);
                    }
                }

                query = String.Format("select tekst FROM dokumentacja WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Dokumentacja = Convert.ToString(row["tekst"]);
                    }
                }

                query = String.Format("select tekst FROM literatura WHERE id = {0}", newPoint.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        newPoint.Literatura = Convert.ToString(row["tekst"]);
                    }
                }
            }

            return newPoint;
        }

        public MapPointViewModel GetPointViewModelById(int id)
        {
            var model = new MapPointViewModel();
            model.Id = id;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var query = String.Format("select miejscowosc, numer_stanowiska, gmina, powiat, wojewodztwo from lokalizacja where id = {0}", model.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        model.Miejscowosc = Convert.ToString(row["miejscowosc"]);
                        model.NumerStanowiska = Convert.ToString(row["numer_stanowiska"]);
                        model.Gmina = Convert.ToString(row["gmina"]);
                        model.Powiat = Convert.ToString(row["powiat"]);
                        model.Wojewodztwo = Convert.ToString(row["wojewodztwo"]);
                    }
                }

                query = String.Format("select funkcja, kultura, chronologia from klasyfikacja where id = {0};", model.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        model.Funkcja = Convert.ToString(row["funkcja"]);
                        model.Kultura = Convert.ToString(row["kultura"]);
                        model.Chronologia = Convert.ToString(row["chronologia"]);
                    }
                }

                query = String.Format("select autor from opracowanie where id = {0};", model.Id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        model.Autor = Convert.ToString(row["autor"]);
                    }
                }
            }
            return model;
        }

        public List<string> GetPointCoordinates()
        {
            List<string> spatialDataList = new List<string>();

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("select ST_AsText(ST_Transform(geometria, 4326)) from wspolrzedne order by id", connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];
                }
            }

            foreach (DataRow row in dataTable.Rows)
            {
                spatialDataList.Add(row[0].ToString());
            }

            return spatialDataList;
        }

        public List<string> GetPolygonCoordinates()
        {
            List<string> spatialDataList = new List<string>();

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("select ST_AsText(ST_Transform(geometria, 4326)) from terenybadawcze order by id", connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];
                }
            }

            foreach (DataRow row in dataTable.Rows)
            {
                spatialDataList.Add(row[0].ToString());
            }

            return spatialDataList;
        }

        public IEnumerable<MapPointViewModel> GetAllPoints()
        {
            var models = new List<MapPointViewModel>(); 
            var count = GetPointsCount();

            for(int i = 1; i < count + 1; i++)
            {
                models.Add(GetPointViewModelById(i));
            }

            return models;
        }

        public string GetPointBySearchString(string searchString)
        {
            var geometry = "";
            var miejscowosc = searchString;
            var azp_stanowiska = 0; //parse to int

            try
            {
                azp_stanowiska = Int32.Parse(searchString);
            }
            catch
            {
                azp_stanowiska = 0;
            }
            
            var azp_obszar = searchString;
            var autor = searchString;

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var query = String.Format("SELECT ST_AsText(geometria) FROM wspolrzedne AS w JOIN lokalizacja AS l ON(w.id = l.id) JOIN opracowanie AS o ON(w.id = o.id) WHERE l.miejscowosc LIKE '{0}' OR l.azp_stanowiska = {1} AND l.azp_obszar LIKE '{2}' OR o.autor LIKE '{3}';", miejscowosc, azp_stanowiska, azp_obszar, autor);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];
                }

                foreach(DataRow row in dataTable.Rows)
                {
                    geometry = row[0].ToString();
                }
            }
                return geometry;
        }

        public Upload GetImageByName(string filename)
        {
            var upload = new Upload();

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var query = String.Format("select tekst from innedane where nazwa LIKE '" + filename + "';");
                byte[] image = null;
                using (var command = new NpgsqlCommand(query, connection))
                {
                    image = (byte[])command.ExecuteScalar();
                }

                if (image.Length > 0)
                {
                    upload.Contents = image;
                    upload.FileName = filename;
                    return upload;
                }
                else
                {
                    return null;
                }
            }
        }
        
        public List<Upload> GetImagesNamesById(int id)
        {
            var filenames = new List<Upload>();
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                var query = String.Format("select id, nazwa from innedane where wspolrzedne_id = {0};", id);
                using (var command = new NpgsqlCommand(query, connection))
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command);
                    dataSet.Reset();
                    da.Fill(dataSet);
                    dataTable = dataSet.Tables[0];

                    foreach (DataRow row in dataTable.Rows)
                    {
                        filenames.Add(new Upload()
                        {
                            Id = Convert.ToInt32(row["id"]),
                            FileName = row["nazwa"].ToString(),
                            WspolrzedneId = id
                        });
                    }
                }
            }

            return filenames;
        }

    }
}
