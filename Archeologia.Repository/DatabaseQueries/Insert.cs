﻿using Archeologia.Repository.Models;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Archeologia.Repository.DatabaseQueries
{
    public class InsertGisData
    {
        private string connectionString;

        public InsertGisData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void InsertMapPointToDatabase(MapPoint mapPoint)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("insert into wspolrzedne (id, geometria, funkcja) values(:id, ST_Transform(ST_GeomFromText(:geometria, 3857), 2180), :funkcja)", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    var geometria = String.Format("POINT({0})", mapPoint.Geometria);
                    command.Parameters.Add(new NpgsqlParameter("geometria", geometria));
                    command.Parameters.Add(new NpgsqlParameter("funkcja", mapPoint.Funkcja));
                    command.ExecuteNonQuery();
                }

                var query = String.Format("insert into kartaewidencyjna(id,a,t,x,n,w,p,l,wspolrzedne_id) values (:id,:a,:t,:x,:n,:w,:p,:l,:wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("a", mapPoint.A));
                    command.Parameters.Add(new NpgsqlParameter("t", mapPoint.T));
                    command.Parameters.Add(new NpgsqlParameter("x", mapPoint.X));
                    command.Parameters.Add(new NpgsqlParameter("n", mapPoint.N));
                    command.Parameters.Add(new NpgsqlParameter("w", mapPoint.W));
                    command.Parameters.Add(new NpgsqlParameter("p", mapPoint.P));
                    command.Parameters.Add(new NpgsqlParameter("l", mapPoint.L));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into lokalizacja(id,numer_stanowiska, nazwa_lokalna, numer_dzialki, identyfikator, wlasciciel, wspolrzedne_id, azp_obszar, azp_stanowiska, miejscowosc, gmina, powiat, wojewodztwo) values (:id, :numer_stanowiska, :nazwa_lokalna, :numer_dzialki, :identyfikator, :wlasciciel, :wspolrzedne_id, :azp_obszar, :azp_stanowiska, :miejscowosc, :gmina, :powiat, :wojewodztwo)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("numer_stanowiska,", mapPoint.NumerStanowiska));
                    command.Parameters.Add(new NpgsqlParameter("nazwa_lokalna", mapPoint.NazwaLokalna));
                    command.Parameters.Add(new NpgsqlParameter("numer_dzialki", mapPoint.NumerDzialki));
                    command.Parameters.Add(new NpgsqlParameter("identyfikator", mapPoint.Identyfikator));
                    command.Parameters.Add(new NpgsqlParameter("wlasciciel", mapPoint.Wlasciciel));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("azp_obszar", mapPoint.AzpObszar));
                    command.Parameters.Add(new NpgsqlParameter("azp_stanowiska", mapPoint.AzpStanowiska));
                    command.Parameters.Add(new NpgsqlParameter("miejscowosc", mapPoint.Miejscowosc));
                    command.Parameters.Add(new NpgsqlParameter("gmina", mapPoint.Gmina));
                    command.Parameters.Add(new NpgsqlParameter("powiat", mapPoint.Powiat));
                    command.Parameters.Add(new NpgsqlParameter("wojewodztwo", mapPoint.Wojewodztwo));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into polozenie(id, jednostka, wspolrzedne_id) values (:id, :jednostka, :wspolrzedne_id)");

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("jednostka", mapPoint.Jednostka));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into utworgeologiczny(id, luzny, zwiezly, torfbagn, okreslenie_specjalistyczne, wspolrzedne_id) values (:id, :luzny, :zwiezly, :torfbagn, :okreslenie_specjalistyczne, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("luzny", mapPoint.Luzny));
                    command.Parameters.Add(new NpgsqlParameter("zwiezly", mapPoint.Zwiezly));
                    command.Parameters.Add(new NpgsqlParameter("torfbagn", mapPoint.Torfbagn));
                    command.Parameters.Add(new NpgsqlParameter("okreslenie_specjalistyczne", mapPoint.OkreslenieSpecjalistyczne));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into dostepnosc(id,niezabudowany,srednio_zabudowany,zabudowany,pole_orne,nieuzytek,laka,sad,park,las,torf,bagno,woda,wspolrzedne_id) values (:id,:niezabudowany,:srednio_zabudowany,-zabudowany,-pole_orne,:nieuzytek,:laka,:sad,:park,:las,:torf,:bagno,:woda,:wspolrzedne_id)");

                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("niezabudowany", mapPoint.Niezabudowany));
                    command.Parameters.Add(new NpgsqlParameter("srednio_zabudowany", mapPoint.SrednioZabudowany));
                    command.Parameters.Add(new NpgsqlParameter("zabudowany", mapPoint.Zabudowany));
                    command.Parameters.Add(new NpgsqlParameter("pole_orne", mapPoint.PoleOrne));
                    command.Parameters.Add(new NpgsqlParameter("nieuzytek", mapPoint.Nieuzytek));
                    command.Parameters.Add(new NpgsqlParameter("laka", mapPoint.Laka));
                    command.Parameters.Add(new NpgsqlParameter("sad", mapPoint.Sad));
                    command.Parameters.Add(new NpgsqlParameter("park", mapPoint.Park));
                    command.Parameters.Add(new NpgsqlParameter("las", mapPoint.Las));
                    command.Parameters.Add(new NpgsqlParameter("torf", mapPoint.Torf));
                    command.Parameters.Add(new NpgsqlParameter("bagno", mapPoint.Bagno));
                    command.Parameters.Add(new NpgsqlParameter("woda", mapPoint.Woda));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into klasyfikacja(id,funkcja,kultura,chronologia,opis,wspolrzedne_id) values(:id,:funkcja,:kultura,:chronologia,:opis,:wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("funkcja", mapPoint.Funkcja));
                    command.Parameters.Add(new NpgsqlParameter("kultura", mapPoint.Kultura));
                    command.Parameters.Add(new NpgsqlParameter("chronologia", mapPoint.Chronologia));
                    command.Parameters.Add(new NpgsqlParameter("opis", mapPoint.Opis));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into opis(id, obserwacja_utrudniona,bez_przeszkodzona,pole_otwarte,pole_zamkniete,wspolrzedne_id,nasycenie_rownomierne,nasycenie_jednocentryczne,nasycenie_wielocentryczne,powierzchnia,gestosc) values(:id,:obserwacja_utrudniona,:bez_przeszkodzona,:pole_otwarte,:pole_zamkniete,:wspolrzedne_id:,nasycenie_rownomierne,:nasycenie_jednocentryczne,:nasycenie_wielocentryczne,:powierzchnia,:gestosc)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("obserwacja_utrudniona", mapPoint.ObserwacjaUtrudniona));
                    command.Parameters.Add(new NpgsqlParameter("bez_przeszkodzona", mapPoint.BezPrzeszk));
                    command.Parameters.Add(new NpgsqlParameter("pole_otwarte", mapPoint.PoleOtwarte));
                    command.Parameters.Add(new NpgsqlParameter("pole_zamkniete", mapPoint.PoleZamkniete));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("nasycenie_rownomierne", mapPoint.NasycenieRownomierne));
                    command.Parameters.Add(new NpgsqlParameter("nasycenie_jednocentryczne", mapPoint.NasycenieJednocentryczne));
                    command.Parameters.Add(new NpgsqlParameter("nasycenie_wielocentryczne", mapPoint.NasycenieWielocentryczne));
                    command.Parameters.Add(new NpgsqlParameter("powierzchnia", mapPoint.Powierzchnia));
                    command.Parameters.Add(new NpgsqlParameter("gestosc", mapPoint.Gestosc));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into zagrozenia(id,tekst,wspolrzedne_id) values(:id,:tekst,:wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("tekst", mapPoint.Zagrozenia));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into wnioski(id, inwentaryzacja, badania, interwencja, wspolrzedne_id) values(:id, :inwentaryzacja, :badania, :interwencja, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("inwentaryzacja", mapPoint.Inwentaryzacja));
                    command.Parameters.Add(new NpgsqlParameter("badania", mapPoint.Badania));
                    command.Parameters.Add(new NpgsqlParameter("interwencja", mapPoint.Interwencja));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into ochrona(id,numer_rejestru,data,park_kulturowy,plan_zagospodarowania,wspolrzedne_id) values(:id, :numer_rejestru, :data, :park_kulturowy, :plan_zagospodarowania, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("numer_rejestru", mapPoint.NumerRejestru));
                    command.Parameters.Add(new NpgsqlParameter("data", mapPoint.DataRejestru));
                    command.Parameters.Add(new NpgsqlParameter("park_kulturowy", mapPoint.ParkKulturowy));
                    command.Parameters.Add(new NpgsqlParameter("plan_zagospodarowania", mapPoint.PlanZagospodarowania));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into opracowanie(id, data, autor, konsultant, konsultantazp, wspolrzedne_id) values(:id, :data, :autor, :konsultant, :konsultantazp, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("data", mapPoint.DataOpracowania));
                    command.Parameters.Add(new NpgsqlParameter("autor", mapPoint.Autor));
                    command.Parameters.Add(new NpgsqlParameter("konsultant", mapPoint.Konsultant));
                    command.Parameters.Add(new NpgsqlParameter("konsultantazp", mapPoint.KonsultantAzp));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into weryfikacja(id, akceptacjawkz, wspolrzedne_id, miejscowosc_data) values(:id, :akceptacjawkz, :wspolrzedne_id, :miejscowosc_data)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("akceptacjawkz", mapPoint.AkceptacjaWkz));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("miejscowosc_data", mapPoint.MiejscowoscData));

                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into zbiory(id, miejsce, numer_inwentarza, wspolrzedne_id) values(:id, :miejsce, :numer_inwentarza, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("miejsce", mapPoint.Miejsce));
                    command.Parameters.Add(new NpgsqlParameter("numer_inwentarza", mapPoint.NumerInwentarza));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into dokumentacja(id, tekst, wspolrzedne_id) values(:id, :tekst, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("tekst", mapPoint.Dokumentacja));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }

                query = String.Format("insert into literatura(id, tekst, wspolrzedne_id) values(:id, :tekst, :wspolrzedne_id)");
                using (var command = new NpgsqlCommand(query, connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPoint.Id));
                    command.Parameters.Add(new NpgsqlParameter("tekst", mapPoint.Literatura));
                    command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", mapPoint.Id));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void InsertPolygonToDatabase(MapPolygon mapPolygon)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("insert into terenybadawcze (id, geometria) values(:id, ST_Transform(ST_GeomFromText(:geometria, 3857), 2180))", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", mapPolygon.Id));
                    var geometria = String.Format("POLYGON(({0}))", mapPolygon.Geometria);
                    command.Parameters.Add(new NpgsqlParameter("geometria", geometria));
                    command.ExecuteNonQuery();
                }
            }
        }

        public void InsertImagesToDatabase(Upload upload)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                {
                    var query = String.Format("insert into innedane(id, tekst, wspolrzedne_id, nazwa) values(:id, :tekst, :wspolrzedne_id, :nazwa)");
                    using (var command = new NpgsqlCommand(query, connection))
                    {
                        command.Parameters.Add(new NpgsqlParameter("id", upload.Id));
                        NpgsqlParameter param = new NpgsqlParameter("tekst", NpgsqlDbType.Bytea);
                        param.Value = upload.Contents;
                        command.Parameters.Add(param);
                        command.Parameters.Add(new NpgsqlParameter("wspolrzedne_id", upload.WspolrzedneId));
                        command.Parameters.Add(new NpgsqlParameter("nazwa", upload.FileName));
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
