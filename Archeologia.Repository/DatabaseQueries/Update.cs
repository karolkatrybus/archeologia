﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace Archeologia.Repository.DatabaseQueries
{
    public class UpdateGisData
    {
        private string connectionString;

        public UpdateGisData(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void UpdateFunkcja(int id, string funkcja)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("update klasyfikacja set funkcja= @funkcja where id= @id", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Integer));
                    command.Parameters.Add(new NpgsqlParameter("funkcja", NpgsqlTypes.NpgsqlDbType.Text));
                    command.Parameters[0].Value = id;
                    command.Parameters[1].Value = funkcja;
                    command.Prepare();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateKultura(int id, string kultura)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("update klasyfikacja set kultura=@kultura where id=@id", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Integer));
                    command.Parameters.Add(new NpgsqlParameter("kultura", NpgsqlTypes.NpgsqlDbType.Text));
                    command.Parameters[0].Value = id;
                    command.Parameters[1].Value = kultura;
                    command.Prepare();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateChronologia(int id, string chronologia)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("update klasyfikacja SET chronologia=@chronologia WHERE id=@id", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Integer));
                    command.Parameters.Add(new NpgsqlParameter("chronologia", NpgsqlTypes.NpgsqlDbType.Text));
                    command.Parameters[0].Value = id;
                    command.Parameters[1].Value = chronologia;
                    command.Prepare();
                    command.ExecuteNonQuery();
                }
            }
        }

        public void UpdateOpis(int id, string opis)
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new NpgsqlCommand("update klasyfikacja SET opis=@opis WHERE id=@id", connection))
                {
                    command.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Integer));
                    command.Parameters.Add(new NpgsqlParameter("opis", NpgsqlTypes.NpgsqlDbType.Text));
                    command.Parameters[0].Value = id;
                    command.Parameters[1].Value = opis;
                    command.Prepare();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
