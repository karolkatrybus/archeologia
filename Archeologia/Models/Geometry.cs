﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Archeologia.Models
{
    public class Geometry
    {
        public List<string> Points;
        public List<string> Polygons;
    }
}
