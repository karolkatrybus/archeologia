﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Archeologia.Repository.DatabaseQueries;
using Archeologia.Repository.Models;
using Microsoft.AspNetCore.Mvc;

namespace Archeologia.Controllers
{
    public class GisDataController : Controller
    {
        private string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=archeologia";
        private InsertGisData insert;
        private SelectGisData select;

        public GisDataController()
        {
            insert = new InsertGisData(connectionString);
            select = new SelectGisData(connectionString);
        }

        public IActionResult List()
        {
            var model = select.GetAllPoints();
            return View(model);
        }

        public IActionResult About(int id)
        {
            var model = select.GetPointViewModelById(id);
            return PartialView(model); 
        }

        public IActionResult Details(int id)
        {
            var model = select.GetPointById(id);
            return PartialView(model); 
        }

        public IActionResult All(int id)
        {
            var model = select.GetPointById(id);
            return View(model);
        }

        public IActionResult Create()
        {
            var id = select.GetPointsCount();
            ViewData["id"] = id + 1;
            return PartialView();
        }

        [HttpPost]
        public IActionResult Create(MapPoint model)
        {
            var id = select.GetPointsCount();
            model.Id = id + 1;

            if (ModelState.IsValid)
            {
                try
                {
                    insert.InsertMapPointToDatabase(model);
                    return RedirectToAction("Index", "Map");
                }
                catch
                {
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
        }

        [HttpPost]
        public IActionResult CreatePolygon(MapPolygon model)
        {
            var id = select.GetPolygonCount();
            model.Id = id + 1;

            try
            {
                insert.InsertPolygonToDatabase(model);
                return RedirectToAction("Index", "Map");
            }
            catch
            {
                return RedirectToAction("Index", "Map");
            }
        }
    }
}