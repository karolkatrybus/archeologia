﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Archeologia.Repository.DatabaseQueries;
using Archeologia.Repository.Models;
using Archeologia.Models;
using Newtonsoft.Json;

namespace Archeologia.Interface.Controllers
{
    public class MapController : Controller
    {
        private string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=archeologia";
        private SelectGisData select;

        public MapController()
        {
            select = new SelectGisData(connectionString);
        }

        public IActionResult Index()
        {
            var model = new Geometry
            {
                Points = select.GetPointCoordinates(),
                Polygons = select.GetPolygonCoordinates()
            };

            return View(model);
        }

        public string Search(string searchString)
        {
            var model = select.GetPointBySearchString(searchString);
            return model;  
        }
    }
}