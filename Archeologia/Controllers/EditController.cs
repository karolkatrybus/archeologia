﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Archeologia.Repository.DatabaseQueries;
using Archeologia.Repository.Models;
using Microsoft.AspNetCore.Mvc;

namespace Archeologia.Controllers
{
    public class EditController : Controller
    {
        private string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=archeologia";
        private InsertGisData insert;
        private SelectGisData select;
        private UpdateGisData update;

        public EditController()
        {
            insert = new InsertGisData(connectionString);
            select = new SelectGisData(connectionString);
            update = new UpdateGisData(connectionString);            
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var model = select.GetPointById(id);
            return PartialView(model);
        }

        [HttpPost]
        public IActionResult Edit(MapPoint model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    EditFunkcja(model.Id, model.Funkcja);
                    EditKultura(model.Id, model.Kultura);
                    EditChronologia(model.Id, model.Chronologia);
                    EditOpis(model.Id, model.Opis);
                    return RedirectToAction("All", "GisData", new { id = model.Id });
                }
                catch
                {
                    return PartialView(model);
                }
            }
            else
            {
                return PartialView(model);
            }
        }

        private void EditFunkcja(int id, string funkcja)
        {
            update.UpdateFunkcja(id, funkcja);
        }
        
        private void EditKultura(int id, string kultura)
        {
            update.UpdateKultura(id, kultura);
        }

        private void EditChronologia(int id, string chronologia)
        {
            update.UpdateChronologia(id, chronologia);
        }

        private void EditOpis(int id, string opis)
        {
            update.UpdateOpis(id, opis);
        }
    }
}