﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Archeologia.Models;
using Archeologia.Repository.DatabaseQueries;
using Microsoft.AspNetCore.Mvc;

namespace Archeologia.Controllers
{
    public class ApiController : Controller
    {
        private string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=archeologia";
        private SelectGisData select;

        public ApiController()
        {
            select = new SelectGisData(connectionString);
        }

        [HttpGet]
        public JsonResult ReturnCoordinatesJSON()
        {
            var model = new Geometry
            {
                Points = select.GetPointCoordinates(),
                Polygons = select.GetPolygonCoordinates()
            };

            return Json(model);
        }
    }
}