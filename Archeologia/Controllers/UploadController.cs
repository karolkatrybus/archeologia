﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Archeologia.Models;
using Archeologia.Repository.DatabaseQueries;
using Archeologia.Repository.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Archeologia.Controllers
{
    public class UploadController : Controller
    {
        private string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=archeologia";
        private InsertGisData insert;
        private SelectGisData select;
        private UpdateGisData update;

        public UploadController()
        {
            insert = new InsertGisData(connectionString);
            select = new SelectGisData(connectionString);
            update = new UpdateGisData(connectionString);
        }

        public IActionResult Index(int id)
        {
            ViewData["id"] = id;
            return View();
        }

        public IActionResult _Gallery(int id)
        {
            var model = select.GetImagesNamesById(id);
            ViewData["id"] = id;
            return PartialView(model);
        }
        
        [HttpPost]
        public IActionResult Upload(ICollection<IFormFile> files, int id)
        {
            foreach(var file in files)
            {
                if (file.Length > 0)
                {
                    var filename = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", filename);

                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    using (var stream = new MemoryStream())
                    {
                        file.CopyTo(stream);
                        var upload = new Upload()
                        {
                            WspolrzedneId = id,
                            FileName = filename,
                            Contents = stream.ToArray(),
                            Id = select.GetImagesCount() + 2
                        };
                        insert.InsertImagesToDatabase(upload);
                    }
                }
            }
            return RedirectToAction("List", "GisData");
        }

        public IActionResult Download(string filename)
        {
            if (filename != null)
            {
                var path = Path.Combine(
                    Directory.GetCurrentDirectory(), "wwwroot", filename);

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    stream.CopyTo(memory);
                }
                memory.Position = 0;
                return File(memory, GetContentType(path), Path.GetFileName(path));
            }
            return null;
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"}
            };
        }
    }
}